﻿using System;
using System.Text.RegularExpressions;

namespace MyBlogAPI.Models
{
    public class Author
    {
        private readonly string _regexNameValidation = @"^.{0,40}$";
        private readonly string _regexEmailValidation = @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$";
        private string _name;
        private string _email;

        public Author()
        {
        }

        public Author(string name, string email)
        {
            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(email))
            {
                throw new ArgumentNullException("Author name and email cannot be null or empty");
            }

            var nameMatch = Regex.Match(name, _regexNameValidation);
            var emailMatch = Regex.Match(name, _regexEmailValidation);
            
            if (!nameMatch.Success || !emailMatch.Success)
            {
                throw new ArgumentException("Author name or email does not match regex validation.");
            }

            _name = name;
            _email = email;
        }

        public bool Equals(Author author)
        {
            if (author._email == this._email && author._name == this._name)
            {
                return true;
            }

            return false;
        }

        public string GetAuthor()
        {
            return _name;
        }

        public string GetAuthorEmail()
        {
            return _email;
        }

        public (string, string) GetValues()
        {
            return (_name, _email);
        }

        public override string ToString()
        {
            return $"{_name}:{_email}";
        }
    }
}
