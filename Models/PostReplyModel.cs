﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyBlogAPI.Models
{
    public class PostReplyModel
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string AuthorEmail { get; set; }
        public string Preamble { get; set; }
        public string BodyText { get; set; }
        public DateTime Date { get; set; }
    }
}
