﻿using System;

namespace MyBlogAPI.Models
{
    public class Post
    {
        public Guid Id { get; private set; }
        public string Title { get; private set; }
        public Author Author { get; private set; }
        public string Preamble { get; private set; }
        public string BodyText { get; private set; }
        public DateTime Date { get; private set; }

        public Post(Guid id, string title, Author author, string preamble, string bodyText, DateTime date)
        {
            if (string.IsNullOrEmpty(id.ToString()))
            {
                throw new ArgumentNullException(nameof(id), "Id cannot be null or empty.");
            }

            if (string.IsNullOrEmpty(title) || title.Length > 50)
            {
                throw new ArgumentNullException(nameof(title), "Title cannot be more than 50 characters and cannot be null or empty.");
            }

            if (author == null || author.Equals(new Author()))
            {
                throw new ArgumentNullException(nameof(author), "Author cannot be null or empty.");
            }

            if (string.IsNullOrEmpty(preamble) || preamble.Length > 250)
            {
                throw new ArgumentNullException(nameof(preamble), "Preamble cannot be more than 250 characters and cannot be null or empty.");
            }

            if (string.IsNullOrEmpty(bodyText))
            {
                throw new ArgumentNullException(nameof(bodyText), "Body text cannot be null or empty.");
            }

            if (date == null || date == default)
            {
                throw new ArgumentNullException(nameof(date), "Date cannot be null or default.");
            }

            Id = id;
            Title = title;
            Author = author;
            Preamble = preamble;
            BodyText = bodyText;
            Date = date;
        }
    }
}
