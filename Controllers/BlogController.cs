﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MyBlogAPI.Models;
using System;
using System.Collections.Generic;

namespace MyBlogAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BlogController : ControllerBase
    {

        private readonly ILogger<BlogController> _logger;
        private readonly IMapper _mapper;
        private List<Post> _posts;

        public BlogController(ILogger<BlogController> logger, IMapper mapper)
        {
            _logger = logger;
            _mapper = mapper;
            _posts = new List<Post>();

            _posts.Add(new Post(
                new Guid("edf9dd5e-69a3-403f-a1e0-7aa0f1ffb974"),
                "Ett exempel på blogginlägg",
                new Author("Victor Bishti", "victor@bishti.se"),
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ac orci id dolor convallis dapibus. Nulla diam augue, tincidunt eu dolor id, auctor posuere nibh. Morbi leo ipsum, elementum ut eleifend sit amet, maximus vel nulla",
                "Aenean elementum, neque eu mattis pulvinar, neque quam fringilla dui, sed molestie augue leo sed dui. Sed velit lacus, sagittis ac pulvinar ut, pretium non risus. Aenean imperdiet finibus turpis id semper. Donec congue posuere nunc, vitae vehicula ex eleifend sed. Phasellus suscipit magna nec velit pharetra, nec porta diam imperdiet. Nunc eget nisi fringilla, fermentum nunc dictum, laoreet nisi. Pellentesque mollis bibendum auctor. Fusce dictum in lectus nec mattis. Vivamus pulvinar lorem pretium, auctor dui vitae, tincidunt turpis. Quisque congue dui a est pellentesque dignissim. Suspendisse potenti. In non urna sodales, vulputate ipsum nec, faucibus ex. Integer non nunc mauris. Aenean congue, dolor ac pharetra hendrerit, arcu turpis convallis ligula, eu vestibulum massa augue id mi. Proin consectetur augue vel lorem pulvinar, efficitur efficitur nibh eleifend. Donec imperdiet turpis at libero consectetur, ut pellentesque arcu placerat. Suspendisse a semper tellus, et ultricies diam. Duis at condimentum velit, sed finibus ante. Praesent quis libero ac ex tempus sodales id id nisi. Vivamus sit amet porttitor lectus. Nam molestie, est a pellentesque ultricies, orci libero aliquam odio, vitae fringilla nibh libero fringilla augue. In aliquet malesuada metus. Morbi ut massa blandit, placerat lacus quis, feugiat mi. Vivamus vitae felis vel orci tempus vestibulum et quis mi. Mauris mattis efficitur tellus, a imperdiet tellus semper malesuada. Quisque felis nisi, imperdiet ac felis dapibus, dapibus pharetra purus. Nam maximus eget turpis in molestie. Maecenas quam neque, elementum id consequat ac, imperdiet at justo. Donec eget accumsan metus, id interdum mi. Vestibulum mollis purus tempor nibh fermentum, sit amet sagittis arcu tempor. Phasellus tempus metus sit amet odio sodales, vitae convallis justo aliquam. Praesent at odio congue, tincidunt turpis quis, hendrerit nunc. Donec turpis neque, fringilla at vestibulum non, mattis quis nibh. Maecenas consectetur sapien in diam accumsan, id consequat neque varius. Donec faucibus ligula sit amet enim egestas, euismod euismod ligula pharetra. Curabitur auctor lectus ut fermentum luctus. Maecenas non tortor mi. Curabitur cursus ante a augue interdum vulputate. Sed nunc nisl, iaculis id arcu eu, dignissim condimentum nisl. Cras sodales eleifend facilisis. Nam urna augue, tempor ac suscipit id, tristique a lectus. Praesent vitae viverra enim. Ut vehicula turpis at eros mollis convallis. Nunc quis velit tincidunt tortor efficitur sollicitudin at id magna.",
                DateTime.Now
            ));

            _posts.Add(new Post(
                new Guid("edf9dd5e-69a3-403f-a1e0-7aa0f1ffb973"),
                "Ett exempel på blogginlägg 2",
                new Author("Victor Bishti", "victor@bishti.se"),
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ac orci id dolor convallis dapibus. Nulla diam augue, tincidunt eu dolor id, auctor posuere nibh. Morbi leo ipsum, elementum ut eleifend sit amet, maximus vel nulla",
                "Aenean elementum, neque eu mattis pulvinar, neque quam fringilla dui, sed molestie augue leo sed dui. Sed velit lacus, sagittis ac pulvinar ut, pretium non risus. Aenean imperdiet finibus turpis id semper. Donec congue posuere nunc, vitae vehicula ex eleifend sed. Phasellus suscipit magna nec velit pharetra, nec porta diam imperdiet. Nunc eget nisi fringilla, fermentum nunc dictum, laoreet nisi. Pellentesque mollis bibendum auctor. Fusce dictum in lectus nec mattis. Vivamus pulvinar lorem pretium, auctor dui vitae, tincidunt turpis. Quisque congue dui a est pellentesque dignissim. Suspendisse potenti. In non urna sodales, vulputate ipsum nec, faucibus ex. Integer non nunc mauris. Aenean congue, dolor ac pharetra hendrerit, arcu turpis convallis ligula, eu vestibulum massa augue id mi. Proin consectetur augue vel lorem pulvinar, efficitur efficitur nibh eleifend. Donec imperdiet turpis at libero consectetur, ut pellentesque arcu placerat. Suspendisse a semper tellus, et ultricies diam. Duis at condimentum velit, sed finibus ante. Praesent quis libero ac ex tempus sodales id id nisi. Vivamus sit amet porttitor lectus. Nam molestie, est a pellentesque ultricies, orci libero aliquam odio, vitae fringilla nibh libero fringilla augue. In aliquet malesuada metus. Morbi ut massa blandit, placerat lacus quis, feugiat mi. Vivamus vitae felis vel orci tempus vestibulum et quis mi. Mauris mattis efficitur tellus, a imperdiet tellus semper malesuada. Quisque felis nisi, imperdiet ac felis dapibus, dapibus pharetra purus. Nam maximus eget turpis in molestie. Maecenas quam neque, elementum id consequat ac, imperdiet at justo. Donec eget accumsan metus, id interdum mi. Vestibulum mollis purus tempor nibh fermentum, sit amet sagittis arcu tempor. Phasellus tempus metus sit amet odio sodales, vitae convallis justo aliquam. Praesent at odio congue, tincidunt turpis quis, hendrerit nunc. Donec turpis neque, fringilla at vestibulum non, mattis quis nibh. Maecenas consectetur sapien in diam accumsan, id consequat neque varius. Donec faucibus ligula sit amet enim egestas, euismod euismod ligula pharetra. Curabitur auctor lectus ut fermentum luctus. Maecenas non tortor mi. Curabitur cursus ante a augue interdum vulputate. Sed nunc nisl, iaculis id arcu eu, dignissim condimentum nisl. Cras sodales eleifend facilisis. Nam urna augue, tempor ac suscipit id, tristique a lectus. Praesent vitae viverra enim. Ut vehicula turpis at eros mollis convallis. Nunc quis velit tincidunt tortor efficitur sollicitudin at id magna.",
                DateTime.Now
            ));
        }

        [HttpGet]
        public ActionResult<IEnumerable<PostReplyModel>> Get()
        {
            return Ok(_mapper.Map<IEnumerable<PostReplyModel>>(_posts));
        }

        [HttpGet("{id:Guid}")]
        public ActionResult<PostReplyModel> Get(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return BadRequest("Id cannot be null or empty.");
            }

            foreach(Post post in _posts){
                if (post.Id.ToString() == id)
                {
                    return Ok(_mapper.Map<PostReplyModel>(post));
                }
            }

            return NotFound();
        }

        [HttpPost]
        public ActionResult<PostReplyModel> Post([FromBody] CreatePostRequestModel request)
        {
            if (request == null)
            {
                return BadRequest("Request cannot be null.");
            }

            try
            {
                _posts.Add(new Post(
                    new Guid(request.Id),
                    request.Title,
                    new Author(request.Author, request.AuthorEmail),
                    request.Preamble,
                    request.BodyText,
                    request.Date
                ));
            }
            catch(Exception exception)
            {
                _logger.LogError("An error occured while trying to save post:", exception);
                return BadRequest("An error occured while trying to save post.");
            }

            return Ok(request);
        }


    }
}
