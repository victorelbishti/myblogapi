﻿using AutoMapper;
using MyBlogAPI.Models;

namespace MyBlogAPI.Mapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<CreatePostRequestModel, Post>()
                .ForMember(d => d.Id, opt => opt.MapFrom(o => o.Id.ToString()))
                .ForMember(d => d.Author, opt => opt.MapFrom(o => new Author(o.Author, o.AuthorEmail)));

            CreateMap<Post, PostReplyModel>()
                .ForMember(d => d.Id, opt => opt.MapFrom(o => o.Id.ToString()))
                .ForMember(d => d.Author, opt => opt.MapFrom(o => o.Author.GetAuthor()))
                .ForMember(d => d.AuthorEmail, opt => opt.MapFrom(o => o.Author.GetAuthorEmail()));
        }
    }
}
